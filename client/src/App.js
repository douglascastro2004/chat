import React, { useState, useEffect } from "react";
import io from "socket.io-client";
import "./App.css";

let socket;
const CONNECTION_PORT = "localhost:3002/";

function App() {
  // Antes do Login
  const [loggedIn, setLoggedIn] = useState(false);
  const [room, setRoom] = useState("");
  const [userName, setUserName] = useState("");

  // Depois do Login
  const [message, setMessage] = useState("");
  const [messageList, setMessageList] = useState([]);

  useEffect(() => {
    socket = io(CONNECTION_PORT);
  }, [CONNECTION_PORT]);

  useEffect(() => {
    socket.on("receive_message", (data) => {
      setMessageList([...messageList, data]);
    });
  });
  const connectToRoom = () => {
    setLoggedIn(true);
    socket.emit("join_room", room);
  };

  const joinRoom = (roomName) => {
    socket.emit("disconnecting");
    socket.emit("join_room", roomName);
    setRoom(roomName);
  }

  const sendMessage = async (e) => {
    e.preventDefault();
    let messageContent = {
      room: room,
      content: {
        author: userName,
        message: message,
      },
    };

    await socket.emit("send_message", messageContent);
    setMessageList([...messageList, messageContent.content]);
    setMessage("");
  };

  return (
    <div className="App">
      {!loggedIn ? (
        <div className="logIn">
          <h1>Crie uma Sala</h1>
          <div className="inputs">
            <input
              type="text"
              placeholder="Nome..."
              aria-required="true"
              onChange={(e) => {
                setUserName(e.target.value);
              }}
            />
            <input
              type="text"
              placeholder="Sala..."
              aria-required="true"
              onChange={(e) => {
                setRoom(e.target.value);
              }}
            />
          </div>
          <button onClick={connectToRoom}>Entre no Chat</button>
        </div>
      ) : (
        <div className="chatContainer">
          <button onClick={() => joinRoom("banana")}>banana</button>
          <button onClick={() => joinRoom("morango")}>morango</button> 

          <div className="messages">
            {messageList.map((val, key) => {
              return (
                <div
                  className="messageContainer"
                  id={val.author === userName ? "You" : "Other"}
                >
                  <div className="messageIndividual">
                    {val.author}: {val.message}
                  </div>
                </div>
              );
            })}
          </div>

          <div className="messageInputs">
          <form className="form" onSubmit={sendMessage}>
            <input
              type="text"
              placeholder="Mensagem..."
              onChange={(e) => {
                setMessage(e.target.value);
              }}
            />
            </form>
          </div>
        </div>
      )}  
    </div>
  );
}

export default App;
